# Introduction

Solarus itself is only a game engine, and does not provide any content. You will need to find or create your own sprites, music, sounds, maps, etc.
